#include<stdio.h>
#include<string.h>
void mygrep(FILE*fp ,char*search)
{
	char line[512];
	while(fgets(line,512,fp)!=NULL)
	{
		if(strstr(line,search))
			printf("%s",line);		
		
	}
}
void myreplace(FILE*fp, char*search,char*replace)
{

    /*
     * Read line from source file and write to destination 
     * file after replacing given word.
     */
    while ((fgets(buffer, BUFFER_SIZE, fPtr)) != NULL)
    {
        // Replace all occurrence of word from current line
        replaceAll(buffer, oldWord, newWord);

        // After replacing write it to temp file.
        fputs(buffer, fTemp);
    }


    /* Close all files to release resource */
    fclose(fPtr);
    fclose(fTemp);


    /* Delete original source file */
    remove(path);

    /* Rename temp file as original file */
    rename("replace.tmp", path);

    printf("\nSuccessfully replaced all occurrences of '%s' with '%s'.", oldWord, newWord);

    return 0;
}



/**
 * Replace all occurrences of a given a word in string.
 */
void replaceAll(char *str, const char *oldWord, const char *newWord);
{
    char *pos, temp[BUFFER_SIZE];
    int index = 0;
    int owlen;

    owlen = strlen(oldWord);


    /*
     * Repeat till all occurrences are replaced. 
     */
    while ((pos = strstr(str, oldWord)) != NULL)
    {
        // Bakup current line
        strcpy(temp, str);

        // Index of current found word
        index = pos - str;

        // Terminate str after word found index
        str[index] = '\0';

        // Concatenate str with new word 
        strcat(str, newWord);
        
        // Concatenate str with remaining words after 
        // oldword found index.
        strcat(str, temp + index + owlen);
    }
}	

int main(int argc,char** argv)
{
	FILE* fp = fopen(argv[1],"r");
	char * search = argv[2];
	char * name = argv[0];
	if(strcmp(name,"./mygrep")==0)
		mygrep(fp,search);
	else if(strcmp(name,"./myreplace")==0)
	{
		char * replace=argv[3];
		myreplace(fp,search,replace);
	}	
	return 0;
}

