#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

float stringToFloat(char* keyString)
{
	int resultint=0;
	float result=0;
	int i=0;
	int count=0;
	bool beforeDec=true;
	int divisor=1;
	while (keyString[i]!='\0')
	{
		if (keyString[i]=='.')
		{
			beforeDec=false;
			i++;
			count++;
			if(count>1)
			{
				printf("Invalid format");
				exit(0);
			}
		}
		if (beforeDec)
		{
			resultint= (resultint*10)+(keyString[i]-'0');
		}
		else
		{
			result= (result*10)+(keyString[i]-'0');
			divisor=divisor*10;
		}
		i++;
	}

	result=result/divisor;
	result=result+resultint;
	return result;
}

int main(int argc, char* argv[])
{
	float f=stringToFloat(argv[1]);
	printf("%f", f);
}
